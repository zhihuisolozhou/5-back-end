import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout: {
      name:'八维创作平台',
      logo:"https://st-gdx.dancf.com/gaodingx/0/uxms/design/20211109-113249-2b41.png?x-oss-process=image/resize,w_300/interlace,1,image/format,webp",
  },
});