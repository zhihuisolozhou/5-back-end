// config/routes.ts

export default [
    {
        path: '/',
        component: '../pages/index',
        name: '工作台', // 兼容此写法
        icon: 'DashboardTwoTone',
    },
    {
        path: '/article',
        name: '文章管理', // 兼容此写法
        icon: 'FormOutlined',
        routes: [
            {
                path: '/article/allarticle',
                component: '../pages/article/allarticle',
                name: "所有文章",
                icon: 'FormOutlined',
            },
            {
                path: '/article/tagarticle',
                component: '../pages/article/tagarticle',
                name: "文章标签",
                icon: 'CopyOutlined'
            },
            {
                path: '/article/typearticle',
                component: '../pages/article/typearticle',
                name: "文章分类",
                icon: 'TagOutlined'
            },
        ]
    },
    {
        path: '../pages/pagemanage',
        component: '../pages/pagemanage',
        name: '页面管理', // 兼容此写法
        icon: 'CopyOutlined'
    },
    {
        path: '../pages/knowledge',
        component: '../pages/knowledge',
        name: '知识小册', // 兼容此写法
        icon: 'BookOutlined'
    },
    {
        path: '../pages/postermanage',
        component: '../pages/postermanage',
        name: '海报管理', // 兼容此写法
        icon: 'CopyOutlined'
    },
    {
        path: '../pages/commentmanage',
        component: '../pages/commentmanage',
        name: '评论管理', // 兼容此写法
        icon: 'MessageOutlined'
    },
    {
        path: '../pages/filemanage',
        component: '../pages/filemanage',
        name: '邮件管理', // 兼容此写法
        icon: 'MailOutlined'
    },
    {
        path: '../pages/filemanagement',
        component: '../pages/filemanagement',
        name: '文件管理', // 兼容此写法
        icon: 'MailOutlined'
       
    },
    {
        path: '../pages/searchrecords',
        component: '../pages/searchrecords',
        name: '搜索记录', // 兼容此写法
        icon: 'SearchOutlined'
    },
    {
        path: '../pages/accessrecords',
        component: '../pages/accessrecords',
        name: '访问统计', // 兼容此写法
        icon: 'ProjectOutlined'
    },
    {
        path: '../pages/usermanage',
        component: '../pages/usermanage',
        name: '用户管理', // 兼容此写法
        icon: 'UserOutlined'
    },
    {
        path: '../pages/systemsettings',
        component: '../pages/systemsettings',
        name: '系统设置', // 兼容此写法
        icon: 'SettingOutlined'
    },
    {
        path: '/login', component: '../pages/login',
        // 新页面打开
        target: '_blank',
        // 不展示顶栏
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
        menuHeaderRender: false,
        // 权限配置，需要与 plugin-access 插件配合使用
        access: 'canRead',
        // 隐藏子菜单
        hideChildrenInMenu: true,
        // 隐藏自己和子菜单
        hideInMenu: true,
        // 在面包屑中隐藏
        hideInBreadcrumb: true,
        // 子项往上提，仍旧展示,
        flatMenu: true,
    },
    {
        path: '/registry', component: '../pages/registry',
        // 新页面打开
        target: '_blank',
        // 不展示顶栏
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
        menuHeaderRender: false,
        // 权限配置，需要与 plugin-access 插件配合使用
        access: 'canRead',
        // 隐藏子菜单
        hideChildrenInMenu: true,
        // 隐藏自己和子菜单
        hideInMenu: true,
        // 在面包屑中隐藏
        hideInBreadcrumb: true,
        // 子项往上提，仍旧展示,
        flatMenu: true,
    },

];